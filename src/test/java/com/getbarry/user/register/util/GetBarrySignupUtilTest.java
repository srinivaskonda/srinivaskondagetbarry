package com.getbarry.user.register.util;

import com.getbarry.user.register.exceptions.InvalidUserInformationException;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class GetBarrySignupUtilTest {

    @Test
    void testFormatName() throws InvalidUserInformationException {
        String userName = "Srinivas Test Konda";
        String resultedName = GetBarrySignupApplicationUtil.formatName(userName);
        assertEquals("Srinivas Konda", resultedName);
    }

    @Test
    void testFormatNameWithEmptyName() {
        Assertions.assertThrows(InvalidUserInformationException.class, () -> {
            GetBarrySignupApplicationUtil.formatName("");
        });
    }

    @Test
    void testAddPrefixToPhoneNumber() throws InvalidUserInformationException {
        String number = "9876 543 21 0";
        String resultedNumber = GetBarrySignupApplicationUtil.addPrefixToPhoneNumber(number);
        assertEquals("+339876543210", resultedNumber);
    }

    @Test
    void getPhoneNumberInFrenchNumberFormatWhereNumberAlreadyFormatted() {
        Assertions.assertThrows(InvalidUserInformationException.class, () -> {
            GetBarrySignupApplicationUtil.addPrefixToPhoneNumber("");
        });
    }
}