package com.getbarry.user.register.controller;

import com.getbarry.user.register.dto.GetBarrySignupDTO;
import com.getbarry.user.register.exceptions.InvalidUserInformationException;
import com.getbarry.user.register.service.impl.GetBarrySignupService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class GetBarrySignupController {

    @Autowired
    private GetBarrySignupService getBarySignupService;


    @PostMapping(value = "/", consumes = "application/json", produces = "application/json")
    public ResponseEntity registration(@RequestBody GetBarrySignupDTO getBarySignUpDTO) {

        try {
            getBarySignupService.signup(getBarySignUpDTO);
        } catch (InvalidUserInformationException e) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
        }

        return ResponseEntity.status(HttpStatus.OK).build();
    }
}

