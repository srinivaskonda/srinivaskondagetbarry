package com.getbarry.user.register.service.impl;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.getbarry.user.register.constants.GetBarrySignupApplicationConstants;
import com.getbarry.user.register.dto.GetBarrySignupDTO;
import com.getbarry.user.register.exceptions.InvalidUserInformationException;
import com.getbarry.user.register.util.GetBarrySignupApplicationUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.task.TaskExecutor;
import org.springframework.stereotype.Component;

@Component
public class GetBarrySignupListener {

    @Autowired
    private TaskExecutor taskExecutor;

    private static final Logger logger = LoggerFactory.getLogger(GetBarrySignupListener.class);


    public void listen(byte[] message) {

        taskExecutor.execute(consumeMessage(message));
    }

    /**
     * Consumes the messages in asynchronous manner
     *
     */
    private Runnable consumeMessage(byte[] message) {
        return () -> {
            String formattedUserPhoneNumber = "";
            String formattedUserName = "";
            GetBarrySignupDTO userInformationDTO = null;
            try {
                //Read the message and convert into DTO
                userInformationDTO = new ObjectMapper().readValue(new String(message), GetBarrySignupDTO.class);
                formattedUserPhoneNumber = GetBarrySignupApplicationUtil.addPrefixToPhoneNumber(userInformationDTO.getPhone());
                formattedUserName = GetBarrySignupApplicationUtil.formatName(userInformationDTO.getFullName());

            } catch (JsonProcessingException e) {
                e.printStackTrace();
                logger.error(GetBarrySignupApplicationConstants.PARSING_ERROR_MESSAGE);
            } catch (InvalidUserInformationException e) {
                logger.error(e.getMessage());
            }

            String responseMessage = String.format(GetBarrySignupApplicationConstants.RESPONSE_MESSAGE, formattedUserName, formattedUserPhoneNumber);
            logger.info(responseMessage);
        };
    }
}
