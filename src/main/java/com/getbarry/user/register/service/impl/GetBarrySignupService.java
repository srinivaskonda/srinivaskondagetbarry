package com.getbarry.user.register.service.impl;

import com.getbarry.user.register.config.GetBarrySignupProperties;
import com.getbarry.user.register.dto.GetBarrySignupDTO;
import com.getbarry.user.register.service.IGetBarrySignupService;
import com.getbarry.user.register.util.GetBarrySignupApplicationUtil;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class GetBarrySignupService implements IGetBarrySignupService {

    @Autowired
    private RabbitTemplate rabbitTemplate;

    @Autowired
    GetBarrySignupProperties getBarrySignupProperties;

    @Override
    public void signup(GetBarrySignupDTO getBarySignupDTO) {

        //Validate the user input before processing
        GetBarrySignupApplicationUtil.validate(getBarySignupDTO);
        rabbitTemplate.convertAndSend(getBarrySignupProperties.getExchangeName(), getBarrySignupProperties.getRoutingKey(), getBarySignupDTO);
    }
}
