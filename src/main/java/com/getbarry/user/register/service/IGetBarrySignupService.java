package com.getbarry.user.register.service;


import com.getbarry.user.register.dto.GetBarrySignupDTO;

public interface IGetBarrySignupService {

    public void signup(GetBarrySignupDTO registrationDTO);
}
