package com.getbarry.user.register.dto;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class GetBarrySignupDTO {

    private String fullName;

    private String phone;

    @JsonCreator
    public GetBarrySignupDTO(
            @JsonProperty(value = "fullName", required = true) String fullName,
            @JsonProperty(value = "phone", required = true) String phone) {
        this.fullName = fullName;
        this.phone = phone;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }
}
