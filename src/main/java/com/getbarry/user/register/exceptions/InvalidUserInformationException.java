package com.getbarry.user.register.exceptions;

public class InvalidUserInformationException extends RuntimeException {

    public InvalidUserInformationException(String message) {
        super(message);
    }
}
