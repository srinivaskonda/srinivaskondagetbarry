package com.getbarry.user.register.util;

import com.getbarry.user.register.constants.GetBarrySignupApplicationConstants;
import com.getbarry.user.register.dto.GetBarrySignupDTO;
import com.getbarry.user.register.exceptions.InvalidUserInformationException;
import com.getbarry.user.register.service.impl.GetBarrySignupListener;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.StringUtils;


public class GetBarrySignupApplicationUtil {

	private static final Logger logger = LoggerFactory.getLogger(GetBarrySignupListener.class);
	
    /**
     * check for the user full name and validate the data
     *
     * @param userName Full name of the user
     * @return return name in required format
     */
    public static String formatName(String userName) throws InvalidUserInformationException {

        if (StringUtils.isEmpty(userName)) {
            String message = String.format(GetBarrySignupApplicationConstants.INVALID_USER_INFO_MESSAGE, "Username");
            throw new InvalidUserInformationException(message);
        }
        String[] nameArray = userName.split(" ");

        if (nameArray.length == 1) {
            return userName;
        } else {
            return nameArray[0] + " " + nameArray[nameArray.length - 1];
        }
    }

    /**
     * Add the prefix to phone number if doesn't found
     *
     * @param phoneNumber Full name of the user
     * @return return phone number with prefix
     */
    public static String addPrefixToPhoneNumber(String phoneNumber) throws InvalidUserInformationException {

        if (StringUtils.isEmpty(phoneNumber)) {
            String message = String.format(GetBarrySignupApplicationConstants.INVALID_USER_INFO_MESSAGE, "Phone Number");
            throw new InvalidUserInformationException(message);
        }
        phoneNumber = phoneNumber.replaceAll("\\s", "");

        if (phoneNumber.startsWith(GetBarrySignupApplicationConstants.EXTENSION_NUMBER)) {
            return phoneNumber;
        } else {
            return GetBarrySignupApplicationConstants.EXTENSION_NUMBER + phoneNumber;
        }
    }

    /**
     * validate the user input
     *
     * @param getBarrySignupDTO DTO that contains user information
     */
    public static void validate(GetBarrySignupDTO getBarrySignupDTO) {

        if (getBarrySignupDTO == null || StringUtils.isEmpty(getBarrySignupDTO.getFullName()) || StringUtils.isEmpty(getBarrySignupDTO.getPhone())) {
        	String error = String.format(GetBarrySignupApplicationConstants.INVALID_USER_INFO_MESSAGE, "Username or Phone number");
        	logger.error(error);
        	throw new InvalidUserInformationException(error);
        }
    }
}
