package com.getbarry.user.register.constants;

/**
 * Configuring application related messages here
 *
 */
public class GetBarrySignupApplicationConstants {
    public static final String RESPONSE_MESSAGE = "User %s with phone %s has just signed up!";
    public static final String EXTENSION_NUMBER = "+33";
    public static final String INVALID_USER_INFO_MESSAGE = "%s is Invalid";
    public static final String PARSING_ERROR_MESSAGE = "Exception while parsing the user information";

}
